MPM Sample Project
========================================================================================================================
This project illustrates how to write a library using mpm.

The advantage to using MPM for a project this simple is that it is _very_ easy for other mpm projects to consume it.

To add this project as a dependency add the following to your direct dependencies:
```
   "mpm_sample_project": {"source": "https://charles_shuller@bitbucket.org/charles_shuller/mpm_sample_project.git",
                          "type": "git",
                          "version": "1.0.0"}
```

It is envisioned that most projects can be distributed as this sort of package.

This project just builds a single library, libmpm_sample_project, with a single exported predicate:
`exported_predicate(InputParameter, OutputParameter)`.

exported_predicate doesn't do anything useful, and is just there so we can easily test that compilation and
linkage happened correctly.   Client code can do things like:

```
main(!IO) :-
  (
    if mpm_sample_project.exported_predicate(1, 1)
    then io.format("Works!\n", [], !IO)
    else io.format("Works, but exported_predicate fails!\n", [], !IO)
  ).
```