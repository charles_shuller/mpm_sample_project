:- module mpm_sample_project.
:- interface.


%%
%% exported_predicate is a simple test predicate that does nothing more than
%% set OutputParameter to InnputParameter.   This has no real utility and
%% is for didactic purposes only.
%%
%% usage:
%%   exported_predicate(InputParameter, OutputParameter).
%%
:- pred exported_predicate(int::in, int::out) is det.






:- implementation.


%%
%% We could have the following as:
%%
%%    exported_predicate(InputParameter, OutputParameter) :-
%%       OutputParameter = InputParameter.
%%
%% but it's far more verbose.
%%
exported_predicate(A, A).
